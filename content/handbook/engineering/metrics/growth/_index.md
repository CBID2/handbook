---
title: "Growth Section Engineering Metrics"
---

These pages are part of our centralized [Engineering Metrics Dashboards](/handbook/engineering/metrics/)

## Group Pages

- [Acquisition Group Dashboards](/handbook/engineering/metrics/growth/acquisition)
- [Activation Group Dashboards](/handbook/engineering/metrics/growth/activation)

{{% engineering/child-dashboards development_section="Growth" %}}
