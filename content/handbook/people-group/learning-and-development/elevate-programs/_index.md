---

title: Elevate Programs
aliases: 
- /handbook/people-group/learning-and-development/elevate

---

Use the links below to navigate to each Elevate program. Here is a useful guide to ensure you're choosing the right program:

| Program Name | Description |
| ----- | -------- |
| [Elevate](https://handbook.gitlab.com/handbook/people-group/learning-and-development/elevate-programs/elevate/) | The next cohort of Elevate begins on 2024-02-12, so if you've been recently enrolled, navigate here. |
| [Elevate Applied](https://handbook.gitlab.com/handbook/people-group/learning-and-development/elevate-programs/elevate-applied/) | Continuous learning and resources for those who've earned their Elevate certification |
| Elevate+ | Coming Soon! |



